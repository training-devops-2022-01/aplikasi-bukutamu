package com.muhardin.endy.training.bukutamu.dao;

import org.springframework.data.repository.CrudRepository;

import com.muhardin.endy.training.bukutamu.entity.Tamu;

public interface TamuDao extends CrudRepository<Tamu, String> {
    
}
