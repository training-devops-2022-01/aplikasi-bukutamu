package com.muhardin.endy.training.bukutamu.dao;

import org.springframework.data.repository.CrudRepository;

import com.muhardin.endy.training.bukutamu.entity.Kota;

public interface KotaDao extends CrudRepository<Kota, Long> {
    Kota findByNama(String nama);
}
