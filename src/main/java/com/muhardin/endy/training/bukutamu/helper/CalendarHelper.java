package com.muhardin.endy.training.bukutamu.helper;

public class CalendarHelper {
    public static Integer jumlahHari(Integer tahun, Integer bulan){
        switch(bulan) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12: 
                return 31;
            case 4:
            case 6:
            case 9:
            case 11:
                return 30;
            default:
                return 0;
        }
    }
}
