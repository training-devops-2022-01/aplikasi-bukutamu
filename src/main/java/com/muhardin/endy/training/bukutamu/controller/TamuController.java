package com.muhardin.endy.training.bukutamu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.support.SessionStatus;

import com.muhardin.endy.training.bukutamu.dao.KotaDao;
import com.muhardin.endy.training.bukutamu.dao.TamuDao;
import com.muhardin.endy.training.bukutamu.entity.Kota;
import com.muhardin.endy.training.bukutamu.entity.Tamu;

@Controller
public class TamuController {

    @Autowired private TamuDao tamuDao;
    @Autowired private KotaDao kotaDao;

    @GetMapping("/api/tamu/")
    @ResponseBody
    public Iterable<Tamu> apiDataTamu(){
        return tamuDao.findAll();
    }

    @GetMapping("/tamu/list")
    public ModelMap dataTamu(){
        return new ModelMap()
        .addAttribute("daftarTamu", tamuDao.findAll());
    }

    @ModelAttribute("daftarKota")
    public Iterable<Kota> dataKota(){
        return kotaDao.findAll();
    }

    @GetMapping("/tamu/form")
    public ModelMap displayFormTamu(){
        return new ModelMap().addAttribute(new Tamu());
    }

    @PostMapping("/tamu/form")
    public String prosesFormTamu(@ModelAttribute Tamu tamu, BindingResult errors, SessionStatus status){

        if(errors.hasErrors()) {
            return "form";
        }

        tamuDao.save(tamu);

        return "redirect:list";
    }
}
