package com.muhardin.endy.training.bukutamu.helper;

public class CalenderHelperDemo {
    public static void main(String[] args) {
        Integer bulan = 12;
        Integer tahun = 2022;
        Integer jumlahHari = CalendarHelper.jumlahHari(tahun, bulan);
        System.out.println("Jumlah hari = "+jumlahHari);
    }
}
